# Test Tools

# Requerimientos

    - Node version minima v18.16.1

# Instalaciones y ejecuciones

1. Instalar playwright

   ```bash
        npm init playwright@latest
   ```
   instalar navegadores
   ```bash
        npx playwright install
   ```

2. Instalar Artillery.io
   ```bash
        npm install -g artillery artillery-engine-playwright
   ```
3. Instalacion de Extension en Visual Code
   - Playwright Test for VSCode

# Ejecución de Tests

1. Comados de ejecución en playwright
    - Generador de codigo Test
   ```bash
       npm run test:codegen <Dominio-url>
   ```
    para el ejemplo el dominio es: localhost:8080

    - Ejecución de Test Playwright
    ```bash
       npm run test:codegen <Dominio-url>
    ```
   
   * * Otros comandos de playwrigth

   ```bash
        npx playwright --version
   ```
        
    * * Se define tamaño del navegador

   ```bash
        npx playwright codegen --viewport-size=800,600
   ```

    * * Se define navegador para dispositivo movil
   ```bash
        npx playwright codegen --device="iPhone 13"
   ```
    * * Se ejecuta test en modo Debug
   ```bash
        npx playwright test productos --project=chromium –debug
   ```

2. Comando de ejecucion de Artillery
   ```bash
       artillery run <DIR-ARCHIVO-*.yml>
   ```
   Ejemplo de dirección del archivo \*.yml desde la ubicación del proyecto Frontend ./test/artillery/test-load.yml
