const { chromium } = require('playwright');

(async () => {
  const browser = await chromium.launch({
    headless: false
  });
  const context = await browser.newContext();
  const page = await context.newPage();
  await page.goto('http://localhost:8088/login');
  await page.getByRole('button', { name: 'Iniciar sesión' }).click();
  await page.getByRole('button', { name: 'Productos', exact: true }).first().click();
  await page.getByRole('button', { name: 'Roles' }).first().click();
  await page.getByRole('button', { name: 'Políticas' }).first().click();
  await page.getByRole('button', { name: 'Módulos' }).first().click();
  await page.getByRole('button', { name: 'Parámetros' }).first().click();
  await page.getByLabel('search').click();
  await page.getByLabel('Buscar parámetro').click();
  await page.getByLabel('Buscar parámetro').fill('backend');
  await page.getByRole('button', { name: 'Albano' }).click();
  await page.getByText('Cerrar sesión').click();
  await page.getByRole('button', { name: 'Aceptar' }).click();
  await context.close();
  await browser.close();
  await page.close();
})();