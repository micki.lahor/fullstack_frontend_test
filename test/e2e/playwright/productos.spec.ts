import { test, expect } from '@playwright/test'
import { numeroAleatorio, palabraAleatoria } from './utils/generador'

test.describe('Test end to end', () => {
  test.beforeEach('login', async ({ page }) => {
    // Runs before each test and signs in each page.
    await page.goto('/login')
    await page.locator('#usuario').fill('ADMINISTRADOR-TECNICO')
    await page.locator('#contrasena').fill('123')
    await page.getByRole('button', { name: 'Iniciar sesión' }).click()
  })
  test('Gestión de Productos', async ({ page, isMobile }) => {
    await test.step('agregar', async () => {
      if (isMobile) await page.getByRole('button', { name: 'menu' }).click()
      await page.getByRole('button', { name: 'Productos', exact: true }).click()
      await page.getByRole('button', { name: 'Agregar' }).click()
      await page.getByLabel('Producto', { exact: true }).click()
      await page.getByLabel('Producto', { exact: true }).fill('gelatia')
      await page.getByLabel('Código').fill('123-wew')
      await page.getByLabel('Precio').fill('2.5')
      await page.getByLabel('Descripción').fill('gelatina sabor limon')
      await page.getByRole('button', { name: 'Guardar' }).press('Enter')
    })
    await test.step('actualizar', async () => {
      await page.locator('#editarRol-1').click()
      await page.getByLabel('Código').fill('dgg-343')
      await page.getByLabel('Producto', { exact: true }).fill('gelatina')
      await page.getByRole('button', { name: 'Guardar' }).click()
      await page.getByText('Registro actualizado con éxito.').click()
      await page.locator('button').filter({ hasText: 'close' }).click()
    })
    await test.step('activar inactivar', async () => {
      await page.locator('#cambiarEstadoProducto-1').click()
      await page.getByRole('button', { name: 'Aceptar' }).click()
      await page.getByRole('button', { name: 'Activar', exact: true }).click()
      await page.getByRole('button', { name: 'Aceptar' }).click()
      expect(
        await page.locator('button').filter({ hasText: 'close' })
      ).toBeVisible()
    })
  })
  test('Gestión de Módulos', async ({ page, isMobile }) => {
    const moduloAleatorio = palabraAleatoria()
    await test.step('agregar', async () => {
      // en caso de ser móvil
      if (isMobile) await page.getByRole('button', { name: 'menu' }).click()
      // Abriendo ruta de módulos
      await page.getByRole('button', { name: 'Módulos', exact: true }).click()
      await page.locator('#agregarModuloSeccion').click()
      await page.getByRole('menuitem', { name: 'Nuevo módulo' }).click()
      await page.locator('#idModulo').click()
      await page.getByRole('option', { name: 'Configuración' }).click()
      await page.locator('#icono').fill('check')
      await page.getByRole('option', { name: 'check', exact: true }).click()
      await page.locator('#nombre').fill(moduloAleatorio)
      await page.locator('#label').fill(moduloAleatorio)
      await page.locator('#url').fill(moduloAleatorio)
      await page.locator('#orden').fill(numeroAleatorio(1, 100).toString())
      await page.locator('#descripcion').fill(moduloAleatorio)
      await page.getByRole('button', { name: 'Guardar' }).click()
    })
    await test.step('filtrar', async () => {
      await page.locator('#accionFiltrarModuloToggle').click()
      await page.locator('#buscar').click()
      await page.locator('#buscar').fill(moduloAleatorio)
      await page.waitForTimeout(3000)
    })
    await test.step('actualizar', async () => {
      await page.getByRole('button', { name: 'Editar' }).first().click()
      const moduloAleatorio2 = palabraAleatoria()

      await page.locator('#icono').fill('check')
      await page.locator('#nombre').fill(moduloAleatorio2)
      await page.locator('#label').fill(moduloAleatorio2)
      await page.locator('#url').fill(moduloAleatorio2)
      await page.locator('#orden').fill(numeroAleatorio(1, 100).toString())
      await page.locator('#descripcion').fill(moduloAleatorio2)
      await page.getByRole('button', { name: 'Guardar' }).click()
      expect(
        await page.locator('button').filter({ hasText: 'close' })
      ).toBeVisible()
    })
  })
  test.afterEach('login', async ({ page }) => {
    await test.step('login', async () => {
      if (
        await page
          .locator('button')
          .filter({ hasText: 'account_circle' })
          .isVisible()
      )
        await page
          .locator('button')
          .filter({ hasText: 'account_circle' })
          .click()

      if (await page.locator('button').filter({ hasText: 'close' }).isVisible())
        await page.locator('button').filter({ hasText: 'close' }).click()

      if (await page.getByRole('button', { name: 'Albano' }).isVisible())
        await page.getByRole('button', { name: 'Albano' }).click()

      await page.getByText('Cerrar sesión').click()
      await page.getByRole('button', { name: 'Aceptar' }).click()
      await page.context().clearCookies()
      await page.close()
    })
  })
})
