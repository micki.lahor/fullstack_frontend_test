# Manual de instalación

## Requisitos

Antes de continuar con la instalación del proyecto, es necesario que ya se hayan realizado las siguientes instalaciones:

1. [NodeJS](https://github.com/nodesource/distributions/blob/master/README.md): Versión 18
2. [NVM](https://github.com/nvm-sh/nvm) Se recomienda NVM solo para ambientes de DESARROLLO.

## Configuración

La configuración de las variables de entorno se encuentra en el archivo .env.sample, el cual se debe renombrar con .env

```bash
cp .env.sample .env
```

El anterior archivo debe contener las siguientes variables cuyos valores van a variar de acuerdo al ambiente. EJEMPLO:

```bash
NEXT_PUBLIC_APP_ENV=development
NEXT_PUBLIC_SITE_NAME="Frontend Full Stack"
NEXT_PUBLIC_BASE_URL="https://dominio-aplicaction/ws/api"
```

- **NEXT_PUBLIC_APP_ENV**: Entorno donde se está ejecutando el proyecto, puede ser `development`, `test`, `production`
- **NEXT_PUBLIC_SITE_NAME**: Nombre de la aplicación web
- **NEXT_PUBLIC_BASE_URL**: URL de backend que consumira la aplicación

## Instalando el proyecto

Siga los siguientes pasos:

### Instalar dependencias

```bash
npm install
```

### Iniciar en modo desarrollo

```bash
npm run dev
```
