import { NextPage } from 'next'
import { ReactNode, useEffect, useState } from 'react'
import { useAlerts, useSession } from '../../common/hooks'
import { useAuth } from '../../context/auth'
import { CasbinTypes } from '../../common/types'
import { Paginacion } from '../../common/components/ui/datatable/Paginacion'
import { imprimir } from '../../common/utils/imprimir'
import {
  InterpreteMensajes,
  delay,
  siteName,
  titleCase,
} from '../../common/utils'
import { Constantes } from '../../config'
import { ProductoCRUDType } from '../../modules/admin/productos/types/productosCRUDTypes'
import { ordenFiltrado } from '../../common/utils/orden'
import { CriterioOrdenType } from '../../common/types/ordenTypes'
import { LayoutUser } from '../../common/components/layouts'
import {
  AlertDialog,
  CustomDataTable,
  CustomDialog,
  IconoTooltip,
} from '../../common/components/ui'
import { FiltroParametros } from '../../modules/admin/parametros/ui/FiltroParametros'
import {
  Button,
  Grid,
  Typography,
  useMediaQuery,
  useTheme,
} from '@mui/material'
import { BotonBuscar } from '../../common/components/ui/botones/BotonBuscar'
import { useRouter } from 'next/router'
import { BotonOrdenar } from '../../common/components/ui/botones/BotonOrdenar'
import { IconoBoton } from '../../common/components/ui/botones/IconoBoton'
import { VistaModalProducto } from '../../modules/admin/productos/ui/ModalProducto'

const Productos: NextPage = () => {
  const [loading, setLoading] = useState<boolean>(true)
  const { Alerta } = useAlerts()
  const [errorParametrosData, setErrorParametrosData] = useState<any>()

  //variables de Data
  const [productosData, setProductosData] = useState<ProductoCRUDType[]>([])
  const [filtroProducto, setFiltroProducto] = useState<string>('')
  const [mostrarFiltroProductos, setMostrarFiltroProductos] = useState(false)
  const [productoEdicion, setProductoEdicion] = useState<ProductoCRUDType>()
  const [mostrarAlertaEstadoProducto, setMostrarAlertaEstadoProducto] =
    useState(false)
  const [modalProductos, setModalProductos] = useState(false)

  // Variables de páginado
  const [limite, setLimite] = useState<number>(10)
  const [pagina, setPagina] = useState<number>(1)
  const [total, setTotal] = useState<number>(0)

  //variables de tema
  const theme = useTheme()
  const xs = useMediaQuery(theme.breakpoints.only('xs'))

  //variables de sesion
  const { sesionPeticion } = useSession()
  const { estaAutenticado, permisoUsuario } = useAuth()
  const router = useRouter()

  // Permisos para acciones
  const [permisos, setPermisos] = useState<CasbinTypes>({
    read: false,
    create: false,
    update: false,
    delete: false,
  })

  //productos de page
  const paginacion = (
    <Paginacion
      pagina={pagina}
      limite={limite}
      total={total}
      cambioPagina={setPagina}
      cambioLimite={setLimite}
    />
  )

  const [ordenCriterios, setOrdenCriterios] = useState<
    Array<CriterioOrdenType>
  >([
    { campo: 'codigo', nombre: 'Código', ordenar: true },
    { campo: 'nombre', nombre: 'Nombre', ordenar: true },
    { campo: 'descripcion', nombre: 'Descripción', ordenar: true },
    { campo: 'estado', nombre: 'Estado', ordenar: true },
    { campo: 'acciones', nombre: 'Acciones' },
  ])

  const editarEstadoProductoModal = async (recurso: ProductoCRUDType) => {
    setProductoEdicion(recurso)
    setMostrarAlertaEstadoProducto(true)
  }

  const obtenerProductosPeticion = async () => {
    try {
      setLoading(true)
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/productos`,
        params: {
          pagina: pagina,
          limite: limite,
          ...(filtroProducto.length == 0 ? {} : { filtro: filtroProducto }),
          ...(ordenFiltrado(ordenCriterios).length == 0
            ? {}
            : {
                orden: ordenFiltrado(ordenCriterios).join(','),
              }),
        },
      })
      setProductosData(respuesta.datos?.filas)
      setTotal(respuesta.datos?.total)
      setErrorParametrosData(null)
    } catch (e) {
      imprimir(`Error al obtener parametros`, e)
      setErrorParametrosData(e)
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: 'error' })
    } finally {
      setLoading(false)
    }
  }

  const cambiarEstadoProductosPeticion = async (producto: ProductoCRUDType) => {
    try {
      setLoading(true)
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/productos/${producto.id}/${
          producto.estado == 'ACTIVO' ? 'inactivacion' : 'activacion'
        }`,
        tipo: 'patch',
      })
      imprimir(`respuesta inactivar rol: ${respuesta}`)
      Alerta({
        mensaje: InterpreteMensajes(respuesta),
        variant: 'success',
      })
      await obtenerProductosPeticion()
    } catch (e) {
      imprimir(`Error al inactivar rol`, e)
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: 'error' })
    } finally {
      setLoading(false)
    }
  }

  const contenidoTabla: Array<Array<ReactNode>> = productosData.map(
    (productoData, indexProducto) => [
      <Typography
        key={`${productoData.id}-${indexProducto}-codigo`}
      >{`${productoData.codigo}`}</Typography>,
      <Typography
        key={`${productoData.id}-${indexProducto}-nombre`}
      >{`${productoData.nombre}`}</Typography>,
      <Typography
        key={`${productoData.id}-${indexProducto}-descripcion`}
      >{`${productoData.descripcion}`}</Typography>,
      <Typography
        key={`${productoData.id}-${indexProducto}-estado`}
      >{`${productoData.estado}`}</Typography>,
      <Grid key={`${productoData.id}-${indexProducto}-acciones`}>
        {permisos.update && (
          <IconoTooltip
            id={`cambiarEstadoProducto-${productoData.id}`}
            titulo={productoData.estado == 'ACTIVO' ? 'Inactivar' : 'Activar'}
            color={productoData.estado == 'ACTIVO' ? 'success' : 'error'}
            accion={async () => {
              await editarEstadoProductoModal(productoData)
            }}
            desactivado={productoData.estado == 'PENDIENTE'}
            icono={productoData.estado == 'ACTIVO' ? 'toggle_on' : 'toggle_off'}
            name={
              productoData.estado == 'ACTIVO'
                ? 'Inactivar Parámetro'
                : 'Activar Parámetro'
            }
          />
        )}
        {permisos.update && (
          <IconoTooltip
            id={`editarRol-${productoData.id}`}
            titulo={'Editar'}
            color={'primary'}
            accion={() => {
              imprimir(`Editaremos`, productoData)
              editarProductoModal(productoData)
            }}
            icono={'edit'}
            name={'Roles'}
          />
        )}
      </Grid>,
    ]
  )

  const agregarProductoModal = () => {
    setProductoEdicion(undefined)
    setModalProductos(true)
  }
  const editarProductoModal = (Rol: ProductoCRUDType) => {
    setProductoEdicion(Rol)
    setModalProductos(true)
  }

  const cerrarModalProducto = async () => {
    setModalProductos(false)
    await delay(500)
    setProductoEdicion(undefined)
  }

  const cancelarAlertaEstadoProducto = async () => {
    setMostrarAlertaEstadoProducto(false)
    await delay(500)
    setProductoEdicion(undefined)
  }

  const aceptarAlertaEstadoProducto = async () => {
    setMostrarAlertaEstadoProducto(false)
    if (productoEdicion) {
      await cambiarEstadoProductosPeticion(productoEdicion)
    }
    setProductoEdicion(undefined)
  }

  const acciones: Array<ReactNode> = [
    <BotonBuscar
      id={'accionFiltrarProductosToggle'}
      key={'accionFiltrarProductosToggle'}
      seleccionado={mostrarFiltroProductos}
      cambiar={setMostrarFiltroProductos}
    />,
    xs && (
      <BotonOrdenar
        id={'ordenarProducto'}
        key={`ordenarProducto`}
        label={'Ordenar producto'}
        criterios={ordenCriterios}
        cambioCriterios={setOrdenCriterios}
      />
    ),
    <IconoTooltip
      id={'actualizarRol'}
      titulo={'Actualizar'}
      key={`accionActualizarRol`}
      accion={async () => {
        await obtenerProductosPeticion()
      }}
      icono={'refresh'}
      name={'Actualizar lista de producto'}
    />,
    permisos.create && (
      <IconoBoton
        id={'agregarProducto'}
        key={'agregarProducto'}
        texto={'Agregar'}
        variante={xs ? 'icono' : 'boton'}
        icono={'add_circle_outline'}
        descripcion={'Agregar producto'}
        accion={() => {
          agregarProductoModal()
        }}
      />
    ),
  ]

  async function definirPermisos() {
    setPermisos(await permisoUsuario(router.pathname))
  }

  useEffect(() => {
    definirPermisos().finally()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [estaAutenticado])

  useEffect(() => {
    if (estaAutenticado) obtenerProductosPeticion().finally(() => {})
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [estaAutenticado, pagina, limite])
  return (
    <>
      <AlertDialog
        isOpen={mostrarAlertaEstadoProducto}
        titulo={'Alerta'}
        texto={`¿Está seguro de ${
          productoEdicion?.estado == 'ACTIVO' ? 'inactivar' : 'activar'
        } a ${titleCase(productoEdicion?.nombre ?? '')} ?`}
      >
        <Button onClick={cancelarAlertaEstadoProducto}>Cancelar</Button>
        <Button onClick={aceptarAlertaEstadoProducto}>Aceptar</Button>
      </AlertDialog>
      <CustomDialog
        isOpen={modalProductos}
        handleClose={cerrarModalProducto}
        title={productoEdicion ? 'Editar producto' : 'Nuevo producto'}
      >
        <VistaModalProducto
          producto={productoEdicion}
          accionCorrecta={() => {
            cerrarModalProducto().finally()
            obtenerProductosPeticion().finally()
          }}
          accionCancelar={cerrarModalProducto}
        />
      </CustomDialog>
      <LayoutUser title={`Productos - ${siteName()}`}>
        <CustomDataTable
          titulo={'Productos'}
          error={!!errorParametrosData}
          cargando={loading}
          acciones={acciones}
          columnas={ordenCriterios}
          cambioOrdenCriterios={setOrdenCriterios}
          paginacion={paginacion}
          contenidoTabla={contenidoTabla}
          filtros={
            mostrarFiltroProductos && (
              <FiltroParametros
                filtroParametro={filtroProducto}
                accionCorrecta={(filtros) => {
                  setPagina(1)
                  setLimite(10)
                  setFiltroProducto(filtros.parametro)
                }}
                accionCerrar={() => {
                  imprimir(`👀 cerrar`)
                }}
              />
            )
          }
        />
      </LayoutUser>
    </>
  )
}

export default Productos
