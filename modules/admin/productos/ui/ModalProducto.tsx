import { useState } from 'react'
import { useAlerts, useSession } from '../../../../common/hooks'
import { ProductoCRUDType } from '../types/productosCRUDTypes'
import { Box, Button, DialogActions, DialogContent, Grid } from '@mui/material'
import { FormInputText } from '../../../../common/components/ui/form'
import ProgresoLineal from '../../../../common/components/ui/progreso/ProgresoLineal'
import { useForm } from 'react-hook-form'
import { InterpreteMensajes, delay } from '../../../../common/utils'
import { Constantes } from '../../../../config'
import { imprimir } from '../../../../common/utils/imprimir'

export interface ModalProductoType {
  producto: ProductoCRUDType | undefined
  accionCorrecta: () => void
  accionCancelar: () => void
}

export const VistaModalProducto = ({
  producto,
  accionCorrecta,
  accionCancelar,
}: ModalProductoType) => {
  const [loadingModal, setLoadingModal] = useState<boolean>(false)

  // Hook para mostrar alertas
  const { Alerta } = useAlerts()

  // Proveedor de la sesión
  const { sesionPeticion } = useSession()

  const { handleSubmit, control } = useForm<ProductoCRUDType>({
    defaultValues: {
      id: producto?.id,
      nombre: producto?.nombre,
      codigo: producto?.codigo,
      precio: producto?.precio,
      descripcion: producto?.descripcion,
    },
  })

  const guardarActualizarProducto = async (data: ProductoCRUDType) => {
    await guardarActualizarProductoPeticion(data)
  }

  const guardarActualizarProductoPeticion = async (
    Producto: ProductoCRUDType
  ) => {
    try {
      setLoadingModal(true)
      await delay(1000)
      const respuesta = await sesionPeticion({
        url: `${Constantes.baseUrl}/productos${
          Producto.id ? `/${Producto.id}` : ''
        }`,
        tipo: !!Producto.id ? 'patch' : 'post',
        body: Producto,
      })
      Alerta({
        mensaje: InterpreteMensajes(respuesta),
        variant: 'success',
      })
      accionCorrecta()
    } catch (e) {
      imprimir(`Error al crear o actualizar rol`, e)
      Alerta({ mensaje: `${InterpreteMensajes(e)}`, variant: 'error' })
    } finally {
      setLoadingModal(false)
    }
  }

  return (
    <form onSubmit={handleSubmit(guardarActualizarProducto)}>
      <DialogContent dividers>
        <Grid container direction={'column'} justifyContent="space-evenly">
          <Grid container direction="row" spacing={{ xs: 2, sm: 1, md: 2 }}>
            <Grid item xs={12} sm={12} md={6}>
              <FormInputText
                id={'producto'}
                control={control}
                name="nombre"
                label="Producto"
                disabled={loadingModal}
                rules={{ required: 'Este campo es requerido' }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={6}>
              <FormInputText
                id={'codigo'}
                control={control}
                name="codigo"
                label="Código"
                disabled={loadingModal}
                rules={{ required: 'Este campo es requerido' }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={6}>
              <FormInputText
                id={'precio'}
                control={control}
                name="precio"
                label="Precio"
                disabled={loadingModal}
                rules={{ required: 'Este campo es requerido' }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={6}>
              <FormInputText
                id={'descripcion'}
                control={control}
                name="descripcion"
                label="Descripción"
                disabled={loadingModal}
                rules={{ required: 'Este campo es requerido' }}
              />
            </Grid>
          </Grid>
          <Box height={'20px'} />
          <ProgresoLineal mostrar={loadingModal} />
        </Grid>
      </DialogContent>
      <DialogActions
        sx={{
          my: 1,
          mx: 2,
          justifyContent: {
            lg: 'flex-end',
            md: 'flex-end',
            xs: 'center',
            sm: 'center',
          },
        }}
      >
        <Button
          variant={'outlined'}
          disabled={loadingModal}
          onClick={accionCancelar}
        >
          Cancelar
        </Button>
        <Button variant={'contained'} disabled={loadingModal} type={'submit'}>
          Guardar
        </Button>
      </DialogActions>
    </form>
  )
}
