export interface ProductoCRUDType {
  id: string
  codigo: string
  nombre: string
  precio: string
  descripcion: string
  estado: string
}
