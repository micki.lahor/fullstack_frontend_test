# Frontend Dip. Full Stack - NextJS/ReactJS

Este proyecto es compatible con el
actual [Backend Full Stack](https://gitlab.com/micki.lahor/fullstack_backend_test) creado con
NestJS

## Tecnologías empleadas

- [NextJS](https://nextjs.org), framework sobre ReactJS.
- [ReactJS](https://es.reactjs.org) como librería para desarrollo frontend.
- [MUI (Material UI)](https://mui.com), librería de componentes UI para ReactJS.
- [React Hook Forms](https://react-hook-form.com), librería con Hooks para la gestión de formularios
- [Axios](https://axios-http.com), para el manejo de peticiones HTTP.
- [ESLint](https://eslint.org), para examinar el código en busca de problemas.

## Estructura general

Para la creación de la estructura general del proyecto base se hizo uso de `Next.js CLI`

## Instalación

Para instalar la aplicación se recomienda revisar el siguiente documento:

> [INSTALL.md](INSTALL.md)

### Ejecutar en modo desarrollo

```
npm run dev
```

### Compilar para producción

```
npm run build
```

### Ejecutar test (e2e)

Los test e2e están escritos en [Playwright](https://playwright.dev)

```
npm run test:e2e
```

Para ver la ejecución de los test, modificar el
archivo [test/e2e/playwright/env.sample](test/e2e/playwright/config/.env.sample) y definir
variables `headless : false` y `slowMo : 400`

Para ejecutar un solo test

```
npx playwright test test/e2e/playwright/usuarios.spec.ts
```

## Documentación

La documentación de los componentes fue elaborada
usando [Storybook](https://storybook.js.org/docs/react/get-started/introduction)

Para generar la documentación ejecutar el siguiente comando:

```bash
npm run storybook
```

## Changelog

1. Generar tag de la versión

   > Cambiar el número de versión en archivo `package.json`

2. Generar tag y archivo CHANGELOG

   ```bash
   # Versión mínima, cambia de 1.1.1 a 1.1.2
   npm run release -- --release-as patch
   ```

   ```bash
   # cambio de versión, cambia de 1.1.1 a 1.2.0
   npm run release
   ```

3. Guardar los tags generados

   ```bash
   git push --follow-tags origin master
   ```
